lzss = require "lzss"
plfile = require "pl.file"

input = assert(plfile.read((arg[1] or arg[0]), true))
output, err = lzss.compress(input)
if not output then
        print("-----------Can't compress")
        return
end

print(#input, #output, #output / #input)

output2 = assert(lzss.decompress(output, #input))

if input ~= output2 then
        print("-------------------------NOT MATCH")
--        plfile.write("enc.dat", output)
--        plfile.write("dec.dat", output2)
--        plfile.write("org.dat", input)
end
