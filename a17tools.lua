local lfs = require "lfs"
local plpath = require "pl.path"
local pldir = require "pl.dir"
local plfile = require "pl.file"
local plpretty = require "pl.pretty"
local lz = require "luazen"
local iconv = require "iconv"

local function alignLength(len, align)
        if len % align == 0 then
                return len
        else
                return len + align - len % align
        end
end

local function makeupSpace(output, supposedSize, curSize, c)
        if curSize < supposedSize then
                output:write(string.rep(c, supposedSize - curSize))
        end 
end

local function packAFS(indir, outpath, force)
        local output = assert(io.open(outpath, "wb"))
        local fsinfo = assert(plfile.read(indir .. "/files.info"))
        local files = assert(plpretty.read(fsinfo))
        local nfile = #files

        local curdir = plpath.currentdir()
        plpath.chdir(indir)
        
        local curoff = alignLength(8 + 8 * #files + 8, 0x800)
        if force then
                for i, f in ipairs(files) do
                        if curoff > f.offset then
                                print(f.name, curoff, f.offset)
                                assert(nil, string.format("ERROR: size overflow: before %s", f.name))
                        end
                        f.size = plpath.getsize(f.name)
                        print(i, f.name, f.offset, f.size)
                        curoff = f.offset + f.size
                end
                if curoff > files.nameOffset then
                        print(curoff, files.nameOffset)
                        assert(nil, "ERROR: size overflow: before NameMetas")
                end
        else
                for i, f in ipairs(files) do
                        f.offset = curoff
                        f.size = plpath.getsize(f.name)
                        print(i, f.name, f.size, f.offset)
                        curoff = alignLength(curoff + f.size, 0x800)
                end
                files.nameOffset = curoff
                files.size = alignLength(curoff + 48 * #files, 0x800)
        end

        output:write("AFS\0")
        output:write(string.pack("<I4", nfile))
        for i, f in ipairs(files) do
                output:write(string.pack("<I4I4", f.offset, f.size))
        end
        output:write(string.pack("<I4I4", files.nameOffset, nfile*48))

        for i, f in ipairs(files) do
                curoff = output:seek()
                makeupSpace(output, f.offset, curoff, "\0")
                local content = plfile.read(f.name, true)
                output:write(content)
        end

        curoff = output:seek()
        makeupSpace(output, files.nameOffset, curoff, "\0")
        for i, f in ipairs(files) do
                output:write(f.name)
                output:write(string.rep("\0", 32 - #f.name))
                output:write(string.pack("<I2I2I2I2I2I2I4",
                        f.year, f.month, f.day, 
                        f.hour, f.min, f.sec,
                        f.usec))
        end

        curoff = output:seek()
        makeupSpace(output, files.size, curoff, "\0")
        output:close()

        plpath.chdir(curdir)
        return true
end

local function unpackAFS(ifn, outdir)
        local input = assert(io.open(ifn, "rb"))
        local magic = input:read(4)
        if magic ~= "AFS\0" then
                return nil, "magic mismatched"
        end
        local filecount = string.unpack("<I4", input:read(4))
        local files = {}
        for i = 1, filecount do
                local f = {}
                f.offset, f.size = string.unpack("<I4I4", input:read(8))
                table.insert(files, f)
        end
        local nameOffset, nameLength = string.unpack("<I4I4", input:read(8))
        input:seek("set", nameOffset)
        for i, f in ipairs(files) do
                f.name, f.year, f.month, f.day, f.hour, f.min, f.sec, f.usec = string.unpack("<c32I2I2I2I2I2I2I4", input:read(48))
                f.atime = os.time(f)
                f.name = string.unpack("<z", f.name)
        end

        pldir.makepath(outdir)
        for i, f in ipairs(files) do
                print(f.name, f.size, os.date("%x %X", f.atime))
                input:seek("set", f.offset)
                local p = outdir .. "/" .. f.name
                local c = input:read(f.size)
                plfile.write(p, c, true)
                lfs.touch(p, f.atime)
        end
        
        files.nameOffset = nameOffset
        files.size = input:seek("end")
        local fsinfo = plpretty.write(files)
        plfile.write(outdir .. "/files.info", fsinfo)
        
        input:close()
        return true
end

local function decodeDATString(str)
        local t = {}
        for i = 1, #str do
                local b = string.byte(str, i)
                if b == 0 then
                        table.insert(t, "\0")
                else
                        table.insert(t, string.char(256 - b))
                end
        end
        return table.concat(t)
end

local encodeDATString = decodeDATString

local function decodeDATInDir(indir)
        for p, m in pldir.dirtree(indir) do
                if not m and string.find(p, "%.DAT$") then
                        print(plpath.basename(p))
                        local outp = p .. ".dec"
                        local s = assert(plfile.read(p, true))
                        local s2 = decodeDATString(s)
                        assert(plfile.write(outp, s2, true))
                end
        end
        return true
end

local function encodeDATInDir(indir)
        for p, m in pldir.dirtree(indir) do
                if not m and string.find(p, "%.DAT%.dec$") then
                        print(plpath.basename(p))
                        local outp = string.sub(p, 1, #p - 4)
                        local s = assert(plfile.read(p, true))
                        local s2 = encodeDATString(s)
                        assert(plfile.write(outp, s2, true))
                end
        end
        return true
end

local BIN_TYPE = 0x01
local FLOW_TYPE = 0x02
local YO_TYPE = 0x03
local EXE_TYPE = 0x04
local DAT_SCENE_TYPE = 0x11
local DAT_CHOICE_TYPE = 0x12
local DAT_SPEAKER_TYPE = 0x13
local DAT_SPEAK_TYPE = 0x14

local CHARS_PER_LINE = 45

local function genBinType(len)
    local s = string.pack("<BH", BIN_TYPE, len)
    return "BIN " .. lz.b64encode(s)
end

local function extStrGeneral(s, pattern)
        local pos = string.find(s, " ")
        if not pos then
            pos = 0
        end
        return string.unpack(pattern, lz.b64decode(string.sub(s, pos + 1)))
end

local function extBinType(s)
        return extStrGeneral(s, "<BH")
end

local function genFlowType(offset, count, index, len)
    local s = string.pack("<BI4BBH", FLOW_TYPE, offset, count, index, len)
    return "FLOW " .. lz.b64encode(s)
end

local function extFlowType(s)
        return extStrGeneral(s, "<BI4BBH")
end

local function genYoType(i, count, j, k, len)
    local s = string.pack("<BBBBBH", YO_TYPE, i, count, j, k, len)
    return "YO " .. lz.b64encode(s)
end

local function extYoType(s)
        return extStrGeneral(s, "<BBBBBH")
end

local function genExeType(pos, oe, len)
    local s = string.pack("<BI4BH", EXE_TYPE, pos, oe, len)
    return "EXE " .. lz.b64encode(s)
end

local function extExeType(s)
        return extStrGeneral(s, "<BI4BH")
end

local function genDatChoiceType(pos, count, index, slen, blen)
    local s = string.pack("<BI4BBHH", DAT_CHOICE_TYPE, pos, count, index, slen, blen)
    return "CHOICE " .. lz.b64encode(s)
end

local function extDatChoiceType(s)
        return extStrGeneral(s, "<BI4BBHH")
end

local function genDatSpeakerType(pos, len)
    local s = string.pack("<BI4H", DAT_SPEAKER_TYPE, pos, len)
    return "SPEAKER " .. lz.b64encode(s)
end

local function extDatSpeakerType(s)
        return extStrGeneral(s, "<BI4H")
end

local function genDatSpeakType(pos, line, len)
    local s = string.pack("<BI4BH", DAT_SPEAK_TYPE, pos, line, len)
    return "SPEAK " .. lz.b64encode(s)
end

local function extDatSpeakType(s)
        return extStrGeneral(s, "<BI4BH")
end

local function genDatSceneType(pos, len)
    local s = string.pack("<BI4H", DAT_SCENE_TYPE, pos, len)
    return "SCENE " .. lz.b64encode(s)
end

local function extDatSceneType(s)
        return extStrGeneral(s, "<BI4H")
end

local function getStringType(s)
    return extStrGeneral(s, "<B")
end

local function getDatType(s)
    return extStrGeneral(s, "<BI4")
end

local function countDatStringLine(s)
    local lineLen = 0
    local offset = 1
    local line = 1
    while offset <= #s do
        local b = string.byte(s, offset)
        if b < 0x80 then
            if string.char(b) == '#' then
                local flag = string.sub(s, offset, offset + 4 - 1)
                if flag ~= "#cr0" and not string.match(flag, "#c[0-9][0-9]") then
                    print("WARNING unknown flag " .. flag)
                end
                if flag == "#cr0" then
                    local el = math.floor(lineLen / CHARS_PER_LINE)
                    line = line + 1 + el
                    lineLen = 0
                end
                offset = offset + 4
            else
                lineLen = lineLen + 1
                offset = offset + 1
            end
        else
            lineLen = lineLen + 2
            offset = offset + 2
        end
    end
    if lineLen > 0 then
        local el = math.floor(lineLen / CHARS_PER_LINE)
        line = line + el
    end

    return line
end

local function fixDATString(s, line)
    if countDatStringLine(s) == line then
        return s
    end
    local ts = {}
    local offset = 1
    local lineLen = 0
    local lineCount = 1
    while offset <= #s do
        local b = string.byte(s, offset)
        if b < 0x80 then
            local us = string.char(b)
            if us == '#' then
                local flag = string.sub(s, offset, offset + 4 - 1)
                if flag ~= "#cr0" and not string.match(flag, "#c[0-9][0-9]") then
                    print("WARNING unknown flag " .. flag)
                end
                if flag ~= "#cr0" then
                    table.insert(ts, flag)
                end
                offset = offset + 4
            else
                table.insert(ts, us)
                lineLen = lineLen + 1
                offset = offset + 1
            end
        else
            local us = string.sub(s, offset, offset + 1)
            table.insert(ts, us)
            lineLen = lineLen + 2
            offset = offset + 2
        end
        if lineLen > CHARS_PER_LINE - 2 then
            table.insert(ts, "#cr0")
            lineCount = lineCount + 1
            lineLen = 0
        end
    end
    for i = lineCount + 1, line do
        table.insert(ts, "#cr0  ")
    end
    
    return table.concat(ts)
end

local function fixEnString(s)
        local ts = {}
        local i = 1
        while i <= #s do
                local b = string.byte(s, i)
                if b == 0x7f or b == 0x00 then
                        table.insert(ts, " ")
                        i = i + 1
                elseif b > 0x7F then
                        local ub = string.sub(s, i, i+1)
                        if ub == "\xcf\xce" then
                                ub = "\x81\x5c"
                        end
                        table.insert(ts, ub)
                        i = i + 2
                else
                        i = i + 1
                        table.insert(ts, string.char(b))
                end
        end
        return table.concat(ts)
end

local function extractDAT(filelist, stringtable, lang, path, jps)
        local p = string.format("script-%s/%s.dec", lang, path)
        local content = plfile.read(p, true)
        print(p)
        
        local offset = 1
        local ts = jps or {}
        local index = 1
        while offset <= #content do
                local cmd = string.byte(content, offset)
                if cmd == 0x1A then
                        offset = offset + 1
                        break
                end
                local subcmd = string.byte(content, offset + 1)
                local blen = string.unpack("<I2", content, offset + 2)
                if cmd == 0x01 and subcmd == 0x1d then
                        local typ, slen = string.unpack("<I4I4", content, offset + 4)
                        if typ & 0xFF00FFFF ~= 0 then
                                io.stderr:write("WARNING type ~= 0x00XX0000")
                        end
                        typ = (typ >> 16) & 0xFF
                        if slen & 0x0000FFFF ~= 0 then
                                io.stderr:write("WARNING length ~= 0xXXXX0000")
                        end
                        slen = slen >> 16
                        local s = string.sub(content, offset + 12, offset + 12 + slen - 1)
                        if typ == 1 then
                            if jps then
                                s = fixEnString(s)
                                local jptype = getStringType(jps[index][1])
                                if jptype == DAT_SPEAKER_TYPE then
                                    table.insert(jps[index], s)
                                elseif jptype == DAT_SPEAK_TYPE then
                                else
                                    return nil, "ERROR should be speaker or speak"
                                end
                            else
                                table.insert(ts, {
                                    genDatSpeakerType(offset - 1, blen),
                                    "L" .. index, 
                                    s })
                            end
                        else
                            if jps then
                                s = fixEnString(s)
                                if getStringType(jps[index][1]) == DAT_SPEAKER_TYPE then
                                    index = index + 1
                                end
                                if getStringType(jps[index][1]) == DAT_SPEAK_TYPE then
                                    table.insert(jps[index], s)
                                else
                                    print(plpretty.write(jps[index]))
                                    print(index, s, getStringType(jps[index][1]), typ)
                                    return nil, "ERROR should be speak"
                                end
                            else
                                local line = countDatStringLine(s)
                                table.insert(ts, {
                                    genDatSpeakType(offset - 1, line, blen),
                                    "L" .. index,
                                    s })
                            end
                        end
                        index = index + 1
                elseif cmd == 0x0c and subcmd == 0x02 then
                        local slen = string.unpack("<I2", content, offset + 4)
                        local s = string.sub(content, offset + 6, offset + 6 + slen - 1)
                        if jps then
                                s = fixEnString(s)
                                if getStringType(jps[index][1]) == DAT_SCENE_TYPE then
                                    table.insert(jps[index], s)
                                else
                                    return nil, "ERROR should be scene"
                                end
                        else
                                table.insert(ts, {
                                        genDatSceneType(offset - 1, blen),
                                        "L" .. index, 
                                        s })
                        end
                        index = index + 1
                elseif cmd == 0x01 and subcmd == 0x1f then
                        local dummy, count = string.unpack("<I4I4", content, offset + 4)
                        if dummy ~= 0x00020000 then
                                io.stderr:write(string.format("WARNING dummy != 0x00020000: 0x%X", dummy))
                        end
                        local pos = offset - 1
                        count = count >> 16
                        local noff = offset + 12
                        for i = 1, count do
                                local slen = string.unpack("<I2", content, noff)
                                local s = string.sub(content, noff + 2, noff + 2 + slen - 1)
                                if jps then
                                    s = fixEnString(s)
                                    if getStringType(jps[index][1]) == DAT_CHOICE_TYPE then
                                        table.insert(jps[index], s)
                                    else
                                        return nil, "ERROR should be choice"
                                    end
                                else
                                    table.insert(ts, {
                                        genDatChoiceType(pos, count, i, slen, blen),
                                        "L" .. index,
                                        s })
                                end
                                index = index + 1

                                noff = noff + 2 + slen
                        end
                end
                offset = offset + 4 + blen
        end
        if offset - 1 ~= #content then
                io.stderr:write(string.format("WARNING size mismatch %s: %d != %d", infile, offset, #content))
        end
        
        if not jps and #ts > 0 then
                table.insert(stringtable, ts)
                table.insert(filelist, { "script", path, #stringtable, 1, 0 })
        end
        
        return ts
end

local function getBinString(content, offset, ls)
        local len 
        if ls == 1 then
                len = string.byte(content, offset)
                offset = offset + 1
        elseif ls == 2 then
                len = string.unpack("<I2", content, offset)
                offset = offset + 2
        else
                return nil
        end
        local s = string.sub(content, offset, offset + len - 1 - 1)
        local ec = string.byte(content, offset + len - 1)
        if ec ~= 0 then
                io.stderr:write(string.format("ERROR string end 0x%X", offset))
                return nil
        end
        offset = offset + len
        
        return s, offset
end

local function extractBin(filelist, stringtable, dir, path)
        local p = string.format("%s-jp/%s", dir, path)
        local content = plfile.read(p, true)
        local count = string.unpack("<I2", content, 1)
        
        local offset = 3
        local index = 1
        local ts = {}
        for i = 1, count do
                local s, noff = getBinString(content, offset, 1)
                offset = noff
                index = index + 1
                
                table.insert(ts, { genBinType(#s), "L" .. i, s })
        end

        if offset - 1 ~= #content then
                io.stderr:write(string.format("WARNING not end: %s: %d, %d\n", infile, offset, #content))
        end

        table.insert(stringtable, ts)
        table.insert(filelist, { dir, path, #stringtable, 1, 0 })
        
        return ts
end

local function extractYoBin(filelist, stringtable, dir, path)
        local p = string.format("%s-jp/%s", dir, path)
        local content = assert(plfile.read(p, true))
        local offset = 1
        local ts = {}
        local blockIndex = 1
        local index = 1
        while true do
                local count = string.byte(content, offset)
                offset = offset + 1
                local noff, s
                
                for i = 1, count do
                        s, noff = getBinString(content, offset, 1)
                        table.insert(ts, { 
                            genYoType(blockIndex, count, i, 1, #s),
                            "L" .. index,
                            s})
                        index = index + 1
                        offset = noff
                        
                        s, noff = getBinString(content, offset, 1)
                        table.insert(ts, { 
                            genYoType(blockIndex, count, i, 2, #s),
                            "L" .. index,
                            s})
                        index = index + 1
                        offset = noff
                        
                        s, noff = getBinString(content, offset, 2)
                        table.insert(ts, { 
                            genYoType(blockIndex, count, i, 3, #s),
                            "L" .. index,
                            s})
                        index = index + 1
                        offset = noff
                end

                if offset > #content then
                        break
                end
                
                blockIndex = blockIndex + 1
        end
        
        table.insert(stringtable, ts)
        table.insert(filelist, { dir, path, #stringtable, 1, 0 })
        
        return ts
end

local function extractFlowBin(filelist, stringtable, dir, path)
        local p = string.format("%s-jp/%s", dir, path)
        local content = assert(plfile.read(p, true))
        local offset = 1
        local ts = {}
        local index = 1
        while true do
                local noff = string.find(content, "AFCB", offset)
                if not noff then
                        break
                end
                local x, y = string.unpack("<I2I2", content, noff + 4)
                local off2 = noff + 8 + x * y * 0xc
                noff = off2
                
                local count = string.unpack("<I4", content, off2)
                off2 = off2 + 4
                for i = 1, count do
                        local len = string.byte(content, off2)
                        off2 = off2 + 1
                        local s = string.sub(content, off2, off2 + len - 1)
                        off2 = off2 + len
                        table.insert(ts, { 
                            genFlowType(noff, count, i, len),
                            "L" .. index,
                            s })
                        index = index + 1
                end

                offset = off2
        end

        table.insert(stringtable, ts)
        table.insert(filelist, { dir, path, #stringtable, 1, 0 })

        return ts
end

local function extractExe(filelist, stringtable, dir, path)
        local p = string.format("%s-jp/%s", dir, path)
        local c = assert(plfile.read(p, true))
        local mp = p .. ".pos"
        local meta = plpretty.read(assert(plfile.read(mp)))
        
        local ts = {}
        local index = 1
        for i, t in ipairs(meta) do
                local offset, finish = t.start, t.finish
                while offset < finish do
                        local eos = string.find(c, "\0", offset + 1);
                        if not eos then
                                break
                        end
                        local s = string.sub(c, offset + 1, eos - 1)
                        table.insert(ts, { 
                            genExeType(offset, 1, #s),
                            "L" .. index,
                            s })
                        offset = eos
                        index = index + 1
                        while string.byte(c, offset + 1) == 0 do
                                offset = offset + 1
                        end
                end
        end
        
        table.insert(stringtable, ts)
        table.insert(filelist, { dir, path, #stringtable, 1, 0 })
        
        return ts
end

local function extractText(indir, outdir)
    local filelist = { }
    local stringtable = {}
    
    local curdir = plpath.currentdir()
    plpath.chdir(indir)
    
    print("SCRIPT/EXE_CEL.AFS/mm_sl_text.bin")
    assert(extractBin(filelist, stringtable, "exe_cel", "mm_sl_text.bin"))
    
    print("SCRIPT/EXE_CEL.AFS/mm_edl_text.bin")
    assert(extractBin(filelist, stringtable, "exe_cel", "mm_edl_text.bin"))
    
    print("SCRIPT/EXE_CEL.AFS/mm_flow_fcb.bin")
    assert(extractFlowBin(filelist, stringtable, "exe_cel", "mm_flow_fcb.bin"))
    
    print("SCRIPT/EXE_CEL.AFS/mm_alb_text.bin")
    assert(extractBin(filelist, stringtable, "exe_cel", "mm_alb_text.bin"))
    
    print("SCRIPT/EXE_CEL.AFS/mm_yo_text.bin")
    assert(extractYoBin(filelist, stringtable, "exe_cel", "mm_yo_text.bin"))
    
    print("Aoishiro.exe")
    assert(extractExe(filelist, stringtable, "exe", "Aoishiro.exe"))
    
    print("AoUtil.exe")
    assert(extractExe(filelist, stringtable, "exe", "AoUtil.exe"))
    
    local files = {}
    for p, m in pldir.dirtree("script-jp") do
        if not m and string.find(p, "%.DAT%.dec$") then
            local ip = plpath.basename(p)
            ip = string.sub(ip, 1, #ip - 4)
            table.insert(files, ip)
        end
    end
    table.sort(files)
    for _, f in ipairs(files) do
        print(f)
        local ts = assert(extractDAT(filelist, stringtable, "jp", f))
        assert(extractDAT(filelist, stringtable, "en", f, ts))
    end
    
    plpath.chdir(curdir)
    pldir.makepath(outdir)
    local index = 1
    local t = {}
    local sum = 0
    local func = function()
        local p = string.format("%s/st%03d-raw.txt", outdir, index)
        plfile.write(p, plpretty.write(t))
        index = index + 1
        t = {}
        sum = 0
    end
    for _, f in ipairs(filelist) do
        local strs = stringtable[f[3]]
        sum = sum + #strs
        table.insert(t, strs)
        f[3] = index
        f[4] = #t
        if sum > 1000 then
            func()
        end
    end
    if sum > 0 then
        func()
    end
    plfile.write(outdir .. "/files.info", plpretty.write(filelist))
    
    return true
end

local function genEmpty(indir)
    local iconv = require "iconv"
    local id = iconv.open("UTF-8", "SHIFT_JIS")
    for p, m in pldir.dirtree(indir) do
        if not m and string.find(p, "raw%.txt$") then
            print(plpath.basename(p))
            local outp = string.sub(p, 1, #p - 7) .. "u8.txt"
            local strss = plpretty.read(assert(plfile.read(p)))
            for i, strs in ipairs(strss) do
                for j, str in ipairs(strs) do
                    str[3] = id:iconv(str[3])
                    if str[4] then
                        str[4] = id:iconv(str[4])
                    end
                    table.insert(str, "-")
                end
            end
            plfile.write(outp, plpretty.write(strss))
        end
    end
    return true
end

local function writeDatChoice(output, strs, from, count, blen)
    local allfit = true
    local minsize = 0
    local fitsize = 0
    local ts = {}
    for i = from, from + count - 1 do
        local str = strs[i]
        local typ, pos, count, index, slen, blen = extDatChoiceType(str[1])
        local s = str[#str]
        if #s > slen then
            allfit = false
        end
        minsize = minsize + 2 + #s
        fitsize = fitsize + 2 + slen
        table.insert(ts, { len = slen, s = str[#str] })
    end
    if allfit then
        for i, t in ipairs(ts) do
            output:write(string.pack("<I2", t.len))
            output:write(t.s)
            makeupSpace(output, t.len, #(t.s), "\0")
        end
        makeupSpace(output, blen - 8, fitsize, "\0")
        return true
    elseif minsize <= blen - 8 then
        for i, t in ipairs(ts) do
            output:write(string.pack("<I2", #(t.s)))
            output:write(t.s)
        end
        makeupSpace(output, blen - 8, minsize, "\0")
        return true
    else
        return false
    end
end

local function writeDatSpeakSpeaker(output, strs, from)
    local str1 = strs[from]
    local str2 = strs[from + 1]
    
    local _, _, blen1 = extDatSpeakerType(str1[1])
    local _, _, line, blen2 = extDatSpeakType(str2[1])
    
    local s1 = str1[#str1]
    local s2 = fixDATString(str2[#str2], line)
    
    if #s1 <= blen1 - 8 and #s2 <= blen2 - 8 then
        output:write(string.pack("<I4I4I4", 
                (blen1 << 16) | 0x1d01,
                1 << 16,
                #(s1) << 16))
        output:write(s1)
        makeupSpace(output, blen1 - 8, #s1, "\0")
        
        output:write(string.pack("<I4I4I4", 
                (blen2 << 16) | 0x1d01,
                0 << 16,
                #(s2) << 16))
        output:write(s2)
        makeupSpace(output, blen2 - 8, #s2, "\0")
        return true
    elseif #s1 + #s2 <= blen1 - 8 + blen2 - 8 then
        output:write(string.pack("<I4I4I4", 
                ((#s1 + 8) << 16) | 0x1d01,
                1 << 16,
                #s1 << 16))
        output:write(s1)
        local remain = blen2 + blen1 - 8 - #s1
        output:write(string.pack("<I4I4I4", 
                (remain << 16) | 0x1d01,
                0 << 16,
                #(s2) << 16))
        output:write(s2)
        makeupSpace(output, remain - 8, #s2, "\0")
        return true
    else
        print(plpretty.write(str1))
        print(plpretty.write(str2))
        print(#s1, #s2, blen1, blen2)
        return nil
    end
end

local function updateDAT(jppath, outpath, strs)
        local content = assert(plfile.read(jppath, true))
        local output = assert(io.open(outpath, "wb"))
        
        local offset = 0
        local index = 1
        while index <= #strs do
                local str = strs[index]
                local typ, pos = getDatType(str[1])
                if offset < pos then
                        output:write(string.sub(content, offset + 1, pos + 1 - 1))
                elseif offset > pos then
                        print("Size mismatched: ", offset, pos, typ)
                        assert(nil)
                end
                
                if typ == DAT_SCENE_TYPE then
                    local _, _, blen = extDatSceneType(str[1])
                    local s = str[#str]
                    if #s + 2 > blen then
                            return nil, string.format("ERROR size overflow: %s, %s, %s, %s, %d, %d", jppath, str[2], str[3], s, #s, blen)
                    end
                    output:write(string.pack("<I4I2", 
                            (blen << 16) | 0x020c,
                            #s))
                    output:write(s)
                    makeupSpace(output, blen - 2, #s, "\0")
                    index = index + 1
                elseif typ == DAT_CHOICE_TYPE then
                    local _, _, count, ii, slen, blen = extDatChoiceType(str[1])
                    output:write(string.pack("<I4I4I4",
                              (blen << 16) | 0x1f01,
                             0x00020000,
                             count << 16))
                    if not writeDatChoice(output, strs, index, count, blen) then
                             return nil, string.format("ERROR size overflow: %s, %s", jppath, str[2])
                    end
                    index = index + count
                elseif typ == DAT_SPEAKER_TYPE then
                    if not writeDatSpeakSpeaker(output, strs, index) then
                            return nil, string.format("ERROR size overflow: %s, %s, %s", jppath, str[3], str[#str])
                    end
                    index = index + 2
                elseif typ == DAT_SPEAK_TYPE then
                    local _, _, line, blen = extDatSpeakType(str[1])
                    local s = str[#str]
                    s = fixDATString(s, line)
                    if #s + 8 > blen then
                            return nil, string.format("ERROR size overflow: %s, %s, %s, %d, %d", jppath, str[3], s, #s, blen)
                    end
                    output:write(string.pack("<I4I4I4", 
                            (blen << 16) | 0x1d01,
                            0,
                            #(s) << 16))
                    output:write(s)
                    makeupSpace(output, blen - 8, #s, "\0")
                    index = index + 1
                else
                    return nil, "ERROR unknown type " .. typ
                end

                offset = output:seek()
        end
        if offset < #content then
                output:write(string.sub(content, offset + 1))
        elseif offset > #content then
                print("Size mismatched: ", #content, output:seek())
                assert(nil)
        end
        output:close()
        
        return true
end

local function writeBinString(output, s, maxlen, ls)
        local len = maxlen + 1
        if ls == 1 then
                output:write(string.char(len))
        elseif ls == 2 then
                output:write(string.pack("<I2", len))
        else
                return nil
        end
        output:write(s)
        
        makeupSpace(output, maxlen, #s, "\0")
        output:write("\0")
end

local function updateBin(jppath, outpath, strs)
        local output = assert(io.open(outpath, "wb"))
        output:write(string.pack("<I2", #strs))

        for i, str in ipairs(strs) do
                local typ, len = extBinType(str[1])
                local s = str[#str]
                if #s > len then
                        return nil, string.format("ERROR size overflow: %s: %d", outpath, str[2])
                end
                writeBinString(output, s, len, 1)
        end

        output:close()

        return true
end

local function updateFlowBin(jppath, outpath, strs)
        local content = assert(plfile.read(jppath, true))
        local output = assert(io.open(outpath, "wb"))

        local index = 1
        while index <= #strs do
                local str = strs[index]
                local typ, offset, count, ii, len = extFlowType(str[1])
                local outoff = output:seek() + 1
                output:write(string.sub(content, outoff, offset))
                output:write(string.pack("<I4", count))
                for i = 1, count do
                    str = strs[index + i - 1]
                    typ, offset, count, ii, len = extFlowType(str[1])
                    local s = str[#str]
                    if #s > len then
                            return nil, string.format("ERROR size overflow: %s, %d", outpath, str[2])
                    end
                    output:write(string.char(len))
                    output:write(s)
                    makeupSpace(output, len, #s, "\0")
                end
                index = index + count
        end

        local outoff = output:seek() + 1
        if outoff <= #content then
                output:write(string.sub(content, outoff))
        end
        output:close()

        return true
end

local function updateYoBin(jppath, outpath, strs)
        local output = assert(io.open(outpath, "wb"))
        local index = 1
        while index <= #strs do
                local str = strs[index]
                local typ, _, count, _, _, len = extYoType(str[1])
                output:write(string.char(count))
                for i = 1, count do
                    local str1 = strs[index]
                    local _, _, _, _, _, len1 = extYoType(str1[1])
                    local s1 = str1[#str1]
                    if len1 < #s1 then
                            return nil, string.format("ERROR size overflow: %s, %s, %s, %d, %d", jppath, str1[2], str1[3], len1, #s1)
                    end
                    -- print(len1, #s1)
                    writeBinString(output, s1, len1, 1)
                    
                    local str2 = strs[index + 1]
                    local _, _, _, _, _, len2 = extYoType(str2[1])
                    local s2 = str2[#str2]
                    if len2 < #s2 then
                            return nil, string.format("ERROR size overflow: %s, %s, %s, %d, %d", jppath, str2[2], str2[3], len2, #s2)
                    end
                    -- print(len2, #s2)
                    writeBinString(output, s2, len2, 1)
                    
                    local str3 = strs[index + 2]
                    local _, _, _, _, _, len3 = extYoType(str3[1])
                    local s3 = str3[#str3]
                    if len3 < #s3 then
                            return nil, string.format("ERROR size overflow: %s, %s, %s, %d, %d", jppath, str3[2], str3[3], len3, #s3)
                    end
                    -- print(len3, #s3)
                    writeBinString(output, s3, len3, 2)
                    
                    index = index + 3
                end
        end
        output:close()
        return true
end

local function updateExe(jppath, outpath, strs)
        local content = assert(plfile.read(jppath, true))
        local output = assert(io.open(outpath, "wb"))
        
        local offset = 0
        for i, str in ipairs(strs) do
                local typ, pos, otherEncode, len = extExeType(str[1])
                local s = str[#str]
                if s == "= =" then
                        s = str[3]
                end
                if pos > offset then
                        output:write(string.sub(content, offset + 1, pos))
                end
                if len < #s then
                        print(str, plpretty.write(str))
                        return nil, string.format("ERROR size overflow: %s, %s, %s, %s, %d, %d", outpath, str[2], str[3], s, len, #s)
                end
                output:write(s)
                makeupSpace(output, len, #s, "\0")
                -- print(outpath, otherEncode, offset, pos, len, #s, "\"" .. str[3] .. "\"", "\"" .. s .. "\"")
                
                offset = output:seek()
        end
        if offset < #content then
                output:write(string.sub(content, offset + 1))
        end
        
        output:close()
        return true
end

local function mergeUpdate(indir, txtdir, mergeAll)
    local filelist = plpretty.read(assert(plfile.read(txtdir .. "/files.info")))
    local stringtable = {}
    
    local getFile = function(i, j)
        local strs = stringtable[i]
        if not strs then
            strs = plpretty.read(assert(plfile.read(string.format("%s/st%03d-cn.txt", txtdir, i))))
            stringtable[i] = strs
        end
        return strs[j]
    end
    
    for i, f in ipairs(filelist) do
        if mergeAll or f[5] == 1 then
            local strs = getFile(f[3], f[4])

            if f[1] == "exe_cel" then
                local jppath = string.format("%s/%s-jp/%s", indir, f[1], f[2])
                local outpath = string.format("%s/%s-cn/%s", indir, f[1], f[2])
                print(jppath, "=>", outpath)
                if f[2] == "mm_flow_fcb.bin" then
                    assert(updateFlowBin(jppath, outpath, strs))
                elseif f[2] == "mm_yo_text.bin" then
                    assert(updateYoBin(jppath, outpath, strs))
                else
                    assert(updateBin(jppath, outpath, strs))
                end
            elseif f[1] == "script" then
                local jppath = string.format("%s/%s-jp/%s.dec", indir, f[1], f[2])
                local outpath = string.format("%s/%s-cn/%s.dec", indir, f[1], f[2])
                print(jppath, "=>", outpath)
                assert(updateDAT(jppath, outpath, strs))
            elseif f[1] == "exe"  then
                local jppath = string.format("%s/%s-cn/%s", indir, f[1], f[2])
                local outpath = string.format("%s/%s-cn/%s", indir, f[1], string.gsub(f[2], ".exe", "-cn.exe"))
                print(jppath, "=>", outpath)
                assert(updateExe(jppath, outpath, strs))
            else
                return nil, "ERROR unrecogniged file " .. plpretty.write(f)
            end
        end
    end
    
    return true
end

local function extDatChoiceTypeOld(s)
        return extStrGeneral(s, "<BI4BBH")
end

local function getBlenOfDatChoice(indir, filename, pos)
        local content = plfile.read(string.format("%s/script-jp/%s.dec", indir, filename), true)
        local cmd = string.byte(content, pos + 1)
        local subcmd = string.byte(content, pos + 2)
        -- print(indir, filename, #content, pos)
        local blen = string.unpack("<I2", content, pos + 3)

        if cmd ~= 0x01 or subcmd ~= 0x1f then
                assert(nil, string.format("Unknown cmd and subcmd: %d, %d", cmd, subcmd))
        end
        return blen
end


local function updateRawText(indir, txtdir)
        local filefmt = "%s/st%03d-u8.txt"
        local filelist = plpretty.read(assert(plfile.read(txtdir .. "/files.info")))
        local stringtable = {}
        
        local getFile = function(i, j)
            local strs = stringtable[i]
            if not strs then
                strs = plpretty.read(assert(plfile.read(string.format(filefmt, txtdir, i))))
                stringtable[i] = strs
            end
            return strs[j]
        end
        
        for _, f in ipairs(filelist) do
                local strs = getFile(f[3], f[4])
    
                local i = 1
                while i <= #strs do
                        local str = strs[i]
                        local typ = getStringType(str[1])
                        local newstr
                        local step = 1
                        if typ == BIN_TYPE then
                                local _, len = extBinType(str[1])
                                newstr = genBinType(len)
                        elseif typ == FLOW_TYPE then
                                local _, offset, count, index, len = extFlowType(str[1])
                                newstr = genFlowType(offset, count, index, len)
                        elseif typ == YO_TYPE then
                                local _, x, count, y, z, len = extYoType(str[1])
                                newstr = genYoType(x, count, y, z, len)
                        elseif typ == EXE_TYPE then
                                local _, pos, oe, len = extExeType(str[1])
                                newstr = genExeType(pos, oe, len)
                        elseif typ == DAT_SCENE_TYPE then
                                local _, pos, len = extDatSceneType(str[1])
                                newstr = genDatSceneType(pos, len)
                        elseif typ == DAT_CHOICE_TYPE then
                                local _, pos, count, index, slen = extDatChoiceTypeOld(str[1])
                                -- print(f[2], count, index, pos, slen)
                                local blen = getBlenOfDatChoice(indir, f[2], pos)
                                for j = 1, count do
                                        typ, pos, count, index, slen = extDatChoiceTypeOld(strs[i + j - 1][1])
                                        newstr = genDatChoiceType(pos, count, index, slen, blen)
                                        if not string.find(newstr, strs[i + j - 1][1], 1, true) then
                                                print("Choice: ", index, count, pos, slen, blen, strs[i + j - 1][1], newstr)
                                        end
                                        strs[i + j - 1][1] = newstr
                                end
                                step = count
                        elseif typ == DAT_SPEAKER_TYPE then
                                local _, pos, len= extDatSpeakerType(str[1])
                                newstr = genDatSpeakerType(pos, len)
                        elseif typ == DAT_SPEAK_TYPE then
                                local _, pos, line, len= extDatSpeakType(str[1])
                                newstr = genDatSpeakType(pos, line, len)
                        else
                                assert(nil, "Unknown type " .. typ)
                        end

                        if step == 1 then
                                if not string.find(newstr, str[1], 1, true) then
                                        print("WARN: ", str[1], newstr)
                                end
                                str[1] = newstr
                        end
                        
                        i = i + step
                end
        end

        for i, strs in ipairs(stringtable) do
                 plfile.write(string.format(filefmt, txtdir, i), plpretty.write(strs))
        end
        
        return true
end

local function updateRawText2(indir, txtdir)
        local filefmt = "%s/st%03d-u8.txt"
        local filelist = plpretty.read(assert(plfile.read(txtdir .. "/files.info")))
        local stringtable = {}
        
        local getFile = function(i, j)
            local strs = stringtable[i]
            if not strs then
                strs = plpretty.read(assert(plfile.read(string.format(filefmt, txtdir, i))))
                stringtable[i] = strs
            end
            return strs[j]
        end
        
        for _, f in ipairs(filelist) do
                local strs = getFile(f[3], f[4])
    
                local i = 1
                while i <= #strs do
                        local str = strs[i]
                        if str[#str] == "- -" and str[3] ~= "- -" then
                                str[#str] = "= ="
                        elseif str[#str] == "-" and str[3] ~= "-" then
                                str[#str] = "= ="
                        elseif str[#str] == "- " and str[3] ~= "- " then
                                str[#str] = "= ="
                        end
                        
                        i = i + 1
                end
        end

        for i, strs in ipairs(stringtable) do
                 plfile.write(string.format(filefmt, txtdir, i), plpretty.write(strs))
        end
        
        return true
end

local function getNewFlowOffset(indir, filename, pos, count)
        local content = plfile.read(string.format("%s/exe_cel-jp/%s", indir, filename), true)
        if string.sub(content, pos, pos + 4 - 1) ~= "AFCB" then
                assert(nil)
        end

        local x, y = string.unpack("<I2I2", content, pos + 4)
        local off2 = pos + 8 + x * y * 0xc
        local c = string.unpack("<I4", content, off2)
        if count ~= c then
                assert(nil, string.format("Mismatched count: %d, %d", count, c))
        end
        local newpos = off2
        local lens = {}
        off2 = off2 + 4
        for i = 1, count do
                local len = string.byte(content, off2)
                table.insert(lens, len)
                off2 = off2 + 1 + len
        end
        return newpos, lens
end

local function updateRawText3(indir, txtdir)
        local filefmt = "%s/st%03d-u8.txt"
        local filelist = plpretty.read(assert(plfile.read(txtdir .. "/files.info")))
        local stringtable = {}
        
        local getFile = function(i, j)
            local strs = stringtable[i]
            if not strs then
                strs = plpretty.read(assert(plfile.read(string.format(filefmt, txtdir, i))))
                stringtable[i] = strs
            end
            return strs[j]
        end

        for _, f in ipairs(filelist) do
                local strs = getFile(f[3], f[4])
    
                local i = 1
                while i <= #strs do
                        local str = strs[i]
                        local typ = getStringType(str[1])
                        local step = 1
                        if typ == FLOW_TYPE then
                                local _, offset, count, index, len = extFlowType(str[1])
                                local newoffset, newlens = getNewFlowOffset(indir, f[2], offset, count)
                                local newcount
                                for j = 1, count do
                                        typ, offset, newcount, index, len = extFlowType(strs[i + j - 1][1])
                                        assert(count == newcount)
                                        assert(index == j)
                                        if newlens[j] ~= len then
                                                print("Mismatched len: ", j, newlens[j], len)
                                                assert(nil)
                                        end
                                        local newstr = genFlowType(newoffset, count, index, len)
                                        print(strs[i + j - 1][1], "=>", newstr, len)
                                        strs[i + j - 1][1] = newstr
                                end 
                                step = count
                        end
                        i = i + step
                end
        end

        for i, strs in ipairs(stringtable) do
                 plfile.write(string.format(filefmt, txtdir, i), plpretty.write(strs))
        end
        
        return true
end

local function writeChars(t, first, second, count)
        for i = 1, count do
                table.insert(t, string.char(first) .. string.char(second))
                second = second + 1
        end
end

local function genJPMap(outfile)
        local t = {}
        writeChars(t, 0x81, 0x40, 63)
        writeChars(t, 0x81, 0x80, 45)
        writeChars(t, 0x81, 0xB8, 8)
        writeChars(t, 0x81, 0xC8, 7)
        writeChars(t, 0x81, 0xDA, 15)
        writeChars(t, 0x81, 0xF0, 8)
        writeChars(t, 0x81, 0xFC, 1)
        writeChars(t, 0x82, 0x4f, 10)
        writeChars(t, 0x82, 0x60, 26)
        writeChars(t, 0x82, 0x81, 26)
        writeChars(t, 0x82, 0x9f, 83)
        writeChars(t, 0x83, 0x40, 63)
        writeChars(t, 0x83, 0x80, 23)
        writeChars(t, 0x83, 0xb6, 1)
        writeChars(t, 0x83, 0xbf, 3)
        writeChars(t, 0x83, 0xd6, 1)
        writeChars(t, 0x84, 0x74, 1)
        writeChars(t, 0x87, 0x40, 30)
        writeChars(t, 0x87, 0x94, 1)
        writeChars(t, 0x87, 0x98, 2)
        writeChars(t, 0x88, 0x9f, 94)
        for i = 0x89, 0x97 do
                writeChars(t, i, 0x40, 63)
                writeChars(t, i, 0x80, 31)
                writeChars(t, i, 0x9f, 94)
        end
        writeChars(t, 0x98, 0x40, 51)
        writeChars(t, 0x98, 0x9f, 94)
        for i = 0x99, 0x9f do
                writeChars(t, i, 0x40, 63)
                writeChars(t, i, 0x80, 31)
                writeChars(t, i, 0x9f, 94)
        end
        for i = 0xe0, 0xe9 do
                writeChars(t, i, 0x40, 63)
                writeChars(t, i, 0x80, 31)
                writeChars(t, i, 0x9f, 94)
        end
        writeChars(t, 0xea, 0x40, 63)
        writeChars(t, 0xea, 0x80, 31)
        writeChars(t, 0xea, 0x9f, 6)

        local s = plpretty.write(t)
        plfile.write(outfile, s)
        
        return true
end

local function loadCharMap(path)
        local t = { from = {}, to = {} }

        if plpath.isfile(path) then
            s = plfile.read(path)
            t.from = plpretty.read(s)
            for i, v in ipairs(t.from) do
                t.to[v] = i
            end
        end

        return t
end

local function updateCNMap(indir, cnfile, updateAll)
        local cnmap = loadCharMap(cnfile)
        local filelist = plpretty.read(assert(plfile.read(indir .. "/files.info")))
        local stringtable = {}
        local getFile = function(i, j)
            local strs = stringtable[i]
            if not strs then
                strs = plpretty.read(assert(plfile.read(string.format("%s/st%03d-u8.txt", indir, i))))
                stringtable[i] = strs
            end
            -- print(i, j, #strs)
            return strs[j]
        end
        
        for _, f in ipairs(filelist) do
            if updateAll or f[5] == 1 then
                local strs = getFile(f[3], f[4])
                for _, str in ipairs(strs) do
                    local s = str[#str]
                    if s == "= =" then
                        s = str[3]
                    end
                    for p, c in utf8.codes(s) do
                            if c > 0xFF then
                                    local uc = utf8.char(c)
                                    if cnmap.to[uc] == nil then
                                            cnmap.to[uc] = true
                                            table.insert(cnmap.from, uc)
                                    end
                            end
                    end
                end
            end
        end
        table.sort(cnmap.from)
        plfile.write(cnfile, plpretty.write(cnmap.from))
        
        return true
end

local bmfcTemplate = [[
# AngelCode Bitmap Font Generator configuration file
fileVersion=1

# font settings
fontName=%s
fontFile=
charSet=134
fontSize=24
aa=1
scaleH=100
useSmoothing=1
isBold=0
isItalic=0
useUnicode=1
disableBoxChars=1
outputInvalidCharGlyph=0
dontIncludeKerningPairs=0
useHinting=1
renderFromOutline=0
useClearType=1

# character alignment
paddingDown=0
paddingUp=0
paddingRight=0
paddingLeft=0
spacingHoriz=0
spacingVert=0
useFixedHeight=0
forceZero=1

# output file
outWidth=768
outHeight=5088
outBitDepth=32
fontDescFormat=0
fourChnlPacked=0
textureFormat=tga
textureCompression=0
alphaChnl=0
redChnl=4
greenChnl=4
blueChnl=4
invA=0
invR=0
invG=0
invB=0

# outline
outlineThickness=0

# selected chars
%s

# imported icon images
]]

local function genBMFC(u8mapfile, output)
        local u8map = loadCharMap(u8mapfile)        
        local ts = {}
        local t = {}
        for i, s in ipairs(u8map.from) do
                table.insert(t, utf8.codepoint(s))
                if #t == 16 then
                        local s = table.concat(t, ",")
                        s = "chars=" .. s
                        table.insert(ts, s)
                        t = {}
                end
        end
        if #t > 0 then
                local s = table.concat(t, ",")
                s = "chars=" .. s
                table.insert(ts, s)
        end
        local cs = table.concat(ts, "\n")
        local content = string.format(bmfcTemplate, "\xD0\xC2\xCB\xCE\xCC\xE5", cs)
        plfile.write(output, content)
        return true
end

local function genCNMapFromFnt(inpath, outpath)
        local input = assert(io.open(inpath, "r"))
        local ts = {}
        for s in input:lines() do
                local t = {}
                t.id, t.x, t.y = string.match(s, "id=(%d+)%s+x=(%d+)%s+y=(%d+)")
                if t.id then
                        table.insert(ts, t)
                end
        end

        table.sort(ts, function(t1, t2)
                return (t1.x + t1.y * 100) < (t2.x + t2.y * 100)
        end)

        local cs = {}
        for i, t in ipairs(ts) do
                cs[i] = utf8.char(t.id)
        end
        
        local s = plpretty.write(cs)
        plfile.write(outpath, s)

        return true
end

local function doEncode(jpmap, cnmap, s)
        local ts = {}
        for p, c in utf8.codes(s) do
                if c <= 0xFF then
                        table.insert(ts, string.char(c))
                elseif c == 65377 then
                        table.insert(ts, string.char(161))
                else
                        local us = utf8.char(c)
                        local index = cnmap.to[us]
                        local js = index and jpmap.from[index]
                        if not index or not js then
                                io.stderr:write(string.format("Unrecognized: %s\n", us))
                                table.insert(ts, "[]")
                        else
                                table.insert(ts, js)
                        end
                end
        end
        return table.concat(ts)
end

local function checkEncode(prefix, utf_str, jp_str, cn_str, jpmap, cnmap)
        if #cn_str > #jp_str then
                local idJP2U = iconv.open("UTF-8", "SHIFT_JIS")
                local index = 1
                for p, c in utf8.codes(utf_str) do
                        local jpb0 = string.byte(jp_str, index)
                        local cnb0 = string.byte(cn_str, index)
                        if c <= 0xFF or c == 65377 then
                                print(p, index, c, idJP2U:iconv(string.pack("<B", jpb0)), jpb0, cnb0)
                                index = index + 1
                        else
                                local jpb1 = string.byte(jp_str, index + 1)
                                local cnb1 = string.byte(cn_str, index + 1)
                                print(p, index, c, idJP2U:iconv(string.pack("<BB", jpb0, jpb1)), jpb0, jpb1, cnb0, cnb1)
                                index = index + 2
                        end
                end
                print("len", #jp_str, #cn_str, jp_str, cn_str)
                assert(nil)
        end
end

local function encode(jpfile, cnfile, indir, usedefault)
        local cnmap = loadCharMap(cnfile)
        local jpmap = loadCharMap(jpfile)
        local idU2JP = iconv.open("SHIFT_JIS", "UTF-8")
        local idU2CN = iconv.open("GB2312", "UTF-8")
        
        local filelist = plpretty.read(assert(plfile.read(indir .. "/files.info")))
        local stringtable = {}
        local getFile = function(i, j)
            local strs = stringtable[i]
            if not strs then
                strs = plpretty.read(assert(plfile.read(string.format("%s/st%03d-u8.txt", indir, i))))
                stringtable[i] = strs
            end
            return strs[j]
        end
        
        for _, f in ipairs(filelist) do
            local strs = getFile(f[3], f[4])
            for _, str in ipairs(strs) do
                local s = str[#str]
                if s == "= =" then
                        if usedefault == "jp" then
                                s = str[3]
                        elseif usedefault == "en" then
                                s = str[#str - 1]
                        end
                end
                local utf_str = str[3]
                str[3] = idU2JP:iconv(str[3])
                if s == "= =" and #s > #(str[3]) then
                        s = string.sub(s, 1, #(str[3]))
                end
                if getStringType(str[1]) == EXE_TYPE then
                    local typ, pos, otherEncode, len = extExeType(str[1])
                    if otherEncode == 1 then
                        s = idU2CN:iconv(s)
                    else
                        s = doEncode(jpmap, cnmap, s)
                    end
                    -- print(f[2], typ, pos, otherEncode, len, "\"" .. str[3] .. "\"", "\"" .. s .. "\"")
                    checkEncode(str[1] .. "(" .. str[2] .. ")", utf_str, str[3], s, jpmap, cnmap)
                else
                    s = doEncode(jpmap, cnmap, s)
                    checkEncode(str[1] .. "(" .. str[2] .. ")", utf_str, str[3], s, jpmap, cnmap)
                end
                str[#str] = s
            end
        end
        
        for i, strs in pairs(stringtable) do
                local p = string.format("%s/st%03d-cn.txt", indir, i)
                plfile.write(p, plpretty.write(strs))
        end
        return true
end

local palette = {
    "\xFF\xFF\xFF\x00",
    "\xFF\xFF\xFF\x33",
    "\xFF\xFF\xFF\x55",
    "\xFF\xFF\xFF\x77",
    "\xFF\xFF\xFF\x99",
    "\xFF\xFF\xFF\xbb",
    "\xFF\xFF\xFF\xdd",
    "\xFF\xFF\xFF\xff",
}

local function font2Tga(infile, outfile, ftype)
        local count, w, h
        if ftype == "2416" then
                count = 6783
                w = 24
                h = 24
        elseif ftype == "2408" then
                count = 95
                w = 12
                h = 24
        else
                return nil, "ERROR: unknown font type: " .. ftype
        end
        local output = assert(io.open(outfile, "wb"))
        local chperrow = 32
        local imgw = w << 5
        local nrows = count >> 5
        if count & 0x1F ~= 0 then
                nrows = nrows + 1
        end
        local imgh = nrows * h
        output:write(string.pack("<I4I4I4I4I2", 0x20000, 0x0, 0x0, imgw | (imgh << 16), 0x2820))

        local content = plfile.read(infile, true)
        local offset = 0x10
        local bytesperline = w >> 1
        for i = 1, nrows do
                for j = 1, h do
                        for k = 1, chperrow do
                                local index = (i - 1) * chperrow + k
                                if index > count then
                                        output:write(string.rep(palette[1], w))
                                else
                                        local lineoff = offset + ((index - 1) * h + j - 1) * bytesperline
                                        for p = 1, bytesperline do
                                                local b = string.byte(content, lineoff + p)
                                                local low = b & 0x0F
                                                local high = b >> 4
                                                output:write(palette[high + 1])
                                                output:write(palette[low + 1])
                                        end
                                end
                        end
                end
        end
        output:close()
        return true
end

local function tga2Font(tgafile, fontfile, ftype)
        local count, w, h
        if ftype == "2416" then
                count = 6783
                w = 24
                h = 24
        elseif ftype == "2408" then
                count = 95
                w = 12
                h = 24
        else
                return nil, "ERROR: unknown font type: " .. ftype
        end
        
        local bytesPerLine = w >> 1
        local charsPerRow = 32
        local bytesPerPixel = 4

        local content = plfile.read(tgafile, true)

        local output = assert(io.open(fontfile, "wb"))
        output:write(string.pack("<I4I4I4I4", 
                count + 1,
                0x80000000 + bytesPerLine,
                w,
                h))

        for i = 1, count do
                local row = ((i - 1) >> 5) + 1
                local col = (i - 1) % charsPerRow + 1

                local rowOff = (row - 1) * charsPerRow * w * h * bytesPerPixel
                local colOff = (col - 1) * w * bytesPerPixel

                for j = 1, h do
                        local lineOff = (j - 1) * charsPerRow * w * bytesPerPixel
                        local offset = 0x12 + rowOff + lineOff + colOff
                        for k = 1, bytesPerLine do
                                local high = string.byte(content, 
                                        offset + (k-1)*2 *bytesPerPixel + 4)
                                local low = string.byte(content, 
                                        offset + ((k-1)*2+1)*bytesPerPixel + 4)
                                low = low >> 5
                                high = high >> 5
                                local by = (high << 4) | low
                                output:write(string.char(by))

                        end
                end
        end

        local curOff = output:seek()
        local size = (count + 1) * bytesPerLine * h
        makeupSpace(output, size, curOff, "\0")
        output:close()

        return true
end

local function readCelHeader(content, off)
        local magic, unk1, nentr, nanim, nimag, unk2, len, unk3 = string.unpack("<c4I4I2I2I2I2I4I4",
                content, off)
        if magic ~= "CP10" then
                return nil, "ERROR magic mismatch " .. magic
        end
        return { nentr = nentr, nanim = nanim, nimg = nimag, len = len }
end

local function readCelEntry(content, offset)
        local e = {}
        e.type = "ENTR"
        e.curOff = offset
        e.nextOff = string.unpack("<I4", content, offset)

        return e
end

local function readCelAnim(content, offset)
        local e = {}
        e.type = "ANIM"
        e.nextOff = string.unpack("<I4", content, offset)

        return e
end

local function readCelImag(content, offset)
        local e = {}
        e.type = "IMAG"
        local nextOff, unk1, w, h, fmt, unk2, compress, len = string.unpack("<I4I4I2I2I4I4I4I4", content, offset)
        
        e.nextOff = nextOff
        e.w, e.h = w, h
        e.fmt = fmt
        e.compress = compress
        e.len = len
        
        if compress == 1 then
                offset = offset + 28
                local magic, olen, clen = string.unpack(">c2I4I4", content, offset)
                if magic ~= "LZ" then
                        return nil, "ERROR LZ magic not match"
                end
                e.olen = olen
                e.clen = clen
        end

        return e
end

local function readCelEndc(content, offset)
        local e = {}
        e.type = "ENDC"
        e.nextOff = string.unpack("<I4", content, offset)

        return e
end

local function extractCelInfo(content)
        local cel = {}

        local offset = 1
        cel.header = assert(readCelHeader(content, offset))
        
        cel.trunks = {}
        offset = 0x18 + 1
        while true do
                local ttype = string.sub(content, offset, offset + 4 - 1)
                offset = offset + 4
                local trnk
                if ttype == "ENTR" then
                        trnk = assert(readCelEntry(content, offset))
                elseif ttype == "ANIM" then
                        trnk = assert(readCelAnim(content, offset))
                elseif ttype == "IMAG" then
                        trnk = assert(readCelImag(content, offset))
                elseif ttype == "ENDC" then
                        trnk = assert(readCelEndc(content, offset))
                else
                        return nil, "ERROR unknown type"
                end
                offset = trnk.nextOff + 1
                table.insert(cel.trunks, trnk)
                if ttype == "ENDC" then
                        break
                end
        end
        return cel
end

local function saveCelToBmp(content, cel, outdir)
        local lzss = require "lzss"
        local curOff = 0
        for i, t in ipairs(cel.trunks) do
                if t.type == "IMAG" then
                        t.opath = tostring(i) .. ".bmp"
                        local opath = string.format("%s/%s", outdir, t.opath)
                        local output = assert(io.open(opath, "wb"))
                        local size = t.w * t.h * 4
                        output:write(string.pack("<c2I4I4I4", "BM", size + 0x8A, 0, 0, 0x8A))
                        output:write(string.pack("<I4I4i4I4I4I4I4I4", 0x7c, t.w, -t.h, 0x00200001, 3, size, 
                                0x0b13, 0x0b13))
                        makeupSpace(output, 0x8A, output:seek(), "\0")
                        
                        local oc
                        if t.compress == 1 then
                                local dc = string.sub(content, curOff + 42 + 1,
                                        curOff + 42 + t.clen)
                                oc = assert(lzss.decompress(dc, t.olen))
                        else
                                oc = string.sub(content, curOff + 32 + 1,
                                        curOff + 32 + t.len)
                        end
                        
                        if t.fmt == 7 then
                                output:write(oc)
                        elseif t.fmt == 5 then
                                t.fmt5dummy = string.unpack("<I4", oc)
                                local palette = {}
                                for m = 1, 256 do
                                        local coloroff = 4 + m * 4 - 4 + 1
                                        local c = string.sub(oc, coloroff, coloroff + 4 - 1)
                                        palette[m] = c
                                end
                                local off = 4 + 1024
                                for m = 1, t.w * t.h do
                                        local inde = string.byte(oc, off + m)
                                        output:write(palette[inde + 1])
                                end
                        else
                                output:close()
                                return nil, "ERROR unsupported format " .. t.fmt
                        end
                        output:close()
                end
                curOff = t.nextOff
        end
        return true
end

local function saveCelToTga(content, cel, outdir)
        local lzss = require "lzss"
        local curOff = 0
        for i, t in ipairs(cel.trunks) do
                if t.type == "IMAG" then
                        t.opath = tostring(i) .. ".tga"
                        local opath = string.format("%s/%s", outdir, t.opath)
                        local output = assert(io.open(opath, "wb"))
                        
                        output:write(string.pack("<I4I4I4I4I2", 0x20000, 0x0, 0x0, t.w | (t.h << 16), 0x2820))
                        
                        local oc
                        if t.compress == 1 then
                                local dc = string.sub(content, curOff + 42 + 1,
                                        curOff + 42 + t.clen)
                                oc = assert(lzss.decompress(dc, t.olen))
                        else
                                oc = string.sub(content, curOff + 32 + 1,
                                        curOff + 32 + t.len)
                        end
                        
                        if t.fmt == 7 then
                                output:write(oc)
                        elseif t.fmt == 5 then
                                t.fmt5dummy = string.unpack("<I4", oc)
                                local palette = {}
                                for m = 1, 256 do
                                        local coloroff = 4 + m * 4 - 4 + 1
                                        local c = string.sub(oc, coloroff, coloroff + 4 - 1)
                                        palette[m] = c
                                end
                                local off = 4 + 1024
                                for m = 1, t.w * t.h do
                                        local inde = string.byte(oc, off + m)
                                        output:write(palette[inde + 1])
                                end
                        else
                                output:close()
                                return nil, "ERROR unsupported format " .. t.fmt
                        end
                        output:close()
                end
                curOff = t.nextOff
        end
        return true
end

local function checkCelFile(path)
        local input = assert(io.open(path, "rb"))
        local magic = input:read(4)
        input:close()

        return (magic == "CP10")
end

local function extractCelFromDir(indir, outdir, typ)
        local files = {}

        pldir.makepath(outdir)

        local curdir = plpath.currentdir()
        plpath.chdir(indir)
        for p, m in pldir.dirtree(".") do
                if not m and checkCelFile(p) then
                        local ip = plpath.basename(p)
                        local content = plfile.read(p, true)
                        local cel = assert(extractCelInfo(content))
                        do
                                files[ip] = cel
                                plpath.chdir(curdir)
                                local odir = outdir .. "/" .. plpath.basename(p)
                                pldir.makepath(odir)
                                if typ == "bmp" then
                                    assert(saveCelToBmp(content, cel, odir))
                                else
                                    assert(saveCelToTga(content, cel, odir))
                                end
                                
                                plpath.chdir(indir)
                        end
                end
        end

        plpath.chdir(curdir)

        local s = plpretty.write(files)
        plfile.write(outdir .. "/files.info", s)

        return true
end

local function mergeCel(infile, tgadir, outfile, cel)
        local lzss = require "lzss"
        
        local cin = assert(io.open(infile, "rb"))
        local cout = assert(io.open(outfile, "wb"))
        
        cout:write(cin:read(24))
        
        for i, trunk in ipairs(cel.trunks) do
                local inoff = cin:seek()
                local outoff = cout:seek()
                local ttype = trunk.type
                if ttype ~= "IMAG" or not trunk.merge then
                        local size = trunk.nextOff - inoff
                        local indata = cin:read(size)
                        if inoff == outoff then
                                cout:write(indata)
                        else
                                outoff = outoff + size
                                cout:write(ttype)
                                cout:write(string.pack("<I4", outoff))
                                cout:write(string.sub(indata, 9, size))
                        end     
                else
                        trunk.compress = 0
                        local idsize = trunk.w * trunk.h * 4
                        local tgapath = string.format("%s/%d.tga", 
                                tgadir, i)
                        local tgadata = assert(plfile.read(tgapath, true))
                        tgadata = string.sub(tgadata, 19, 18 + idsize)
                        local zipdata
                        if trunk.compress == 1 then
                                zipdata = lzss.compress(tgadata)
                                idsize = 10 + #zipdata
                        end
                        
                        local size = idsize + 32
                        size = alignLength(size, 0x100)

                        local imgheader = cin:read(32)
                        cout:write("IMAG")
                        cout:write(string.pack("<I4", outoff + size))
                        cout:write(string.sub(imgheader, 9, 16))
                        cout:write(string.pack("<I4", 0x07))
                        cout:write(string.sub(imgheader, 21, 24))
                        cout:write(string.pack("<I4", trunk.compress))
                        cout:write(string.pack("<I4", idsize))
                        
                        if trunk.compress == 1 then
                                cout:write(string.pack(">c2I4I4", "LZ", #tgadata, #zipdata))
                                cout:write(zipdata)
                        else
                                cout:write(tgadata)
                        end
                        makeupSpace(cout, size, idsize + 32, "\0")
                end
                cin:seek("set", trunk.nextOff)
        end
        local insize = cin:seek("end")
        local outsize = cout:seek()
        
        if outsize ~= insize then
                cout:seek("set", 16)
                cout:write(string.pack("<I4", outsize - 24))
        end
        cout:close()
        
        return true
end

local function mergeCelToDir(indir, tgadir, outdir, mergeAll)
        local p = tgadir .. "/files.info"
        local s = plfile.read(p)
        local files = plpretty.read(s)

        pldir.makepath(outdir)

        for p, cel in pairs(files) do
                if cel.merge or mergeAll then
                        local ipath = indir .. "/" .. p
                        local opath = outdir .. "/" .. p
                        
                        mergeCel(ipath, tgadir .. "/" .. p, opath, cel)
                end
        end

        return true
end

return {
        unpackAFS = unpackAFS,
        packAFS = packAFS,
        
        encodeDATInDir = encodeDATInDir,
        decodeDATInDir = decodeDATInDir,
        
        extractText = extractText, 
        genEmpty = genEmpty,
        mergeUpdate = mergeUpdate,
        
        genJPMap = genJPMap,
        updateCNMap = updateCNMap,
        genBMFC = genBMFC,
        genCNMapFromFnt = genCNMapFromFnt,
        encode = encode,
        
        font2Tga = font2Tga,
        tga2Font = tga2Font,
        
        extractCelFromDir = extractCelFromDir,
        mergeCelToDir = mergeCelToDir,

        updateRawText = updateRawText3,
}
