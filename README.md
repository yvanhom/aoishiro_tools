# 文本汉化流程

## 准备

从游戏目录下将以下文件拷贝到 packs-jp 下：

* SCRIPT/SCRIPT.AFS
* SCRIPT/EXE_CEL.AFS
* SCRIPT/FONT.AFS
* Aoishiro.exe
* AoiUtil.exe

解包 SCRIPT.AFS 到 test/script-jp:
```
lua a17rt.lua unpackAFS packs-jp/SCRIPT.AFS test/script-jp
```

解密 test/script-jp 中的文件，产生一堆 *.dec 文件：
```
lua a17rt.lua decodeDATInDir test/script-jp
```

解包 EXE_CEL.AFS 到 test/exe_cel-jp：
```
lua a17rt.lua unpackAFS packs-jp/EXE_CEL.AFS test/exe_cel-jp
```

解包 FONT.AFS 到 test/font-jp：
```
lua a17rt.lua unpackAFS packs-jp/FONT.AFS test/font-jp
```

生成 jp.map：
```
lua a17rt.lua genJPMap test/font-jp/jp.map 
```

拷贝一份给汉化用 test/script-cn、test/exe_cel-cn、test/font-cn。

复制已提取的游戏文本所在的文件夹 strings 到 test/working，于是 test/working 下有大量 *-raw.txt、*-u8.txt 的文件。

拷贝 staff 到 test/exe-cn，staff 下的 Aoishiro.exe 和 AoiUtil.exe 是使用 [Resource Hacker](http://www.angusj.com/resourcehacker/) 对原游戏文件进行汉化修改得到的。

准备工作完成。

### 翻译

对 test/working 下的 *-u8.txt 文件进行编辑，以进行翻译，将 "= =" 修改为翻译的文本即可。

比如将以下：
```
    {
      "BIN ARAA",
      "L1",
      "それは物語の欠片",
      "= ="
    },
```
改成：
```
    {
      "BIN ARAA",
      "L1",
      "それは物語の欠片",
      "传奇物语的碎片"
    },
```

有以下几点需要注意：

* 汉化后的文本长度最好不要超过原来的长度
* 多行的文本使用 [[]] 圈起来
* 文本中 # 开头的有特殊的文本控制作用，可以保留，比如 `#c00` 和 `#c31` 等
* EXE 为 Aoishiro.exe、AoiUtil.exe 内的文本
* FLOW 为分歧图中的场景名
* BIN
* YO 为词典
* SPEAKER 为正在说话的人
* SPEAK   为所说的话
* SCENE   为场景名称
* CHOICE  为选项
* **记得备份汉化文本，避免误删除，导致辛苦工作的成果丢失**

### 打包

生成 cn.map：
```
lua a17rt.lua updateCNMap test/working test/font-cn/cn.map updateAll
```

由 cn.map 生成 cn.bmfc：
```
lua a17rt.lua genBMFC test/font-cn/cn.map test/font-cn/cn.bmfc
```

使用 [bmfont64](https://www.angelcode.com/products/bmfont/) 生成字体图像文件 cn_0.tga、cn.fnt：
```
bmfont64.exe -c test\font-cn\cn.bmfc -o test\font-cn\cn.fnt
```

转化图像文件 cn_0.tga 成游戏支持的 2bt 文件：
```
lua a17rt.lua tga2Font test/font-cn/cn_0.tga test/font-cn/font2416.2bt 2416
```

可以打包字体文件 FONT.AFS:
```
lua a17rt.lua packAFS test/font-cn/ packs-cn/FONT.AFS force
```

根据 cn.fnt 重新生成 cn2.map:
```
lua a17rt.lua genCNMapFromFnt test/font-cn/cn.fnt test/font-cn/cn2.map
```

利用 jp.map、cn2.map 对游戏文件进行编码，test/working 下将产生一堆 *-cn.txt 文件，记得使用的是 **cn2.map**：
```
lua a17rt.lua encode test/font-jp/jp.map test/font-cn/cn2.map test/working
```

合并文本到二进制文件中，将更新 test/script-cn、test/exe_cel-cn、test/exe-cn 下对应的文件：
```
lua a17rt.lua mergeUpdate test test/working updateAll

```

拷贝 Aoishiro-cn.exe、AoiUtil-cn.exe 到 packs-cn 下。

打包 EXE_CEL.AFS:
```
lua a17rt.lua packAFS test/exe_cel-cn packs-cn/EXE_CEL.AFS force
```

加密 test/script-cn 下文件：
```
lua a17rt.lua encodeDATInDir test/script-cn
```

打包 SCRIPT.AFS：
```
lua a17rt.lua packAFS test/script-cn packs-cn/SCRIPT.AFS force
```

将 packs-cn 下的文件拷贝的游戏目录下:

* Aoishiro-cn.exe
* AoiUtil.exe
* SCRIPT/SCRIPT.AFS
* SCRIPT/FONT.AFS
* SCRIPT/EXE_CEL.AFS

其他说明：

* 打包过程中如果报错，可能是翻译后的文本超过原文本
* 打包字体时，可以更改英文字体 font-cn/font2408.2bt，也可以使用英文版本下的英文字体

## 附加：文本提取流程

需要额外解压英文版本：
```
lua a17rt.lua unpackAFS packs-en/SCRIPT.AFS test/script-en
lua a17rt.lua decodeDATInDir test/script-en
lua a17rt.lua unpackAFS packs-en/EXE_CEL.AFS test/exe_cel-en
lua a17rt.lua unpackAFS packs-en/FONT.AFS test/font-en
```

提取文本，将在 test/working2 下产生一堆 *-raw.txt：
```
lua a17rt.lua extractText test test/working2
```

将文本转换成 utf-8 格式，将生成一堆 *-u8.txt：
```
lua a17rt.lua genEmpty test/working2
```

可能存在一些无用的文本，需要翻译成跟原文一样的内容，否则程序不会执行。strings 文件夹下的是经过筛选的，删除了无用的文本。
