#!/bin/bash

lua a17rt.lua updcnmapfromdir exe_cel-cn-txt font/cn.map
lua a17rt.lua updcnmapfromdir script-cn-txt font/cn.map
lua a17rt.lua updcnmapfromdir exe-cn-txt font/cn.map

lua a17rt.lua genbmfc font/cn.map test/cn.bmfc
bmfont -c test/cn.bmfc -o test/cn.fnt
lua a17rt.lua gencnmapfromfnt test/cn.fnt font/cn2.map
diff font/cn.map font/cn2.map
mv font/cn2.map font/cn.map

bmfont -c font/en.bmfc -o test/en.fnt
lua a17rt.lua tga2font test/en_0.tga test/en.2bt 2408
mv test/en.2bt test/font-cn/font2408.2bt

lua a17rt.lua packafs test/font-cn test/FONT.AFS true
mv test/FONT.AFS ../SCRIPT/FONT.AFS
