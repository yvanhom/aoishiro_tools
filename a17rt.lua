local a17 = require "a17tools"

local cmdlist = {
        [ "packAFS"] = { cmd = "packAFS", nargs = 2, usage = "indir outpath" },
        [ "unpackAFS"] = { cmd = "unpackAFS", nargs = 2, usage = "infile outdir" },
        
        [ "encodeDATInDir"] = { cmd = "encodeDATInDir", nargs = 1, usage = "dir" },
        [ "decodeDATInDir"] = { cmd = "decodeDATInDir", nargs = 1, usage = "dir" },
        
        [ "extractText"] = { cmd = "extractText", nargs = 2, usage = "indir outdir" },
        [ "genEmpty"] = { cmd = "genEmpty", nargs = 1, usage = "indir" },
        [ "genJPMap"] = { cmd = "genJPMap", nargs = 1, usage = "outfile" },
        [ "updateCNMap"] = { cmd = "updateCNMap", nargs = 2, usage = "indir cnfile [updateAll]" },
        [ "genBMFC"] = { cmd = "genBMFC", nargs = 2, usage = "cnfile outfile" },
        [ "genCNMapFromFnt"] = { cmd = "genCNMapFromFnt", nargs = 2, usage = "infile outfile" },
        [ "encode"] = { cmd = "encode", nargs = 3, usage = "jpmap cnmap indir [default=jp/en]" },
        [ "mergeUpdate"] = { cmd = "mergeUpdate", nargs = 2, usage = "indir txtdir [mergeAll]" },
        
        [ "font2Tga"] = { cmd = "font2Tga", nargs = 3, usage = "infile outfile type" },
        [ "tga2Font"] = { cmd = "tga2Font", nargs = 3, usage = "infile outfile type" },
        
        [ "extractCelFromDir"] = { cmd = "extractCelFromDir", nargs = 2, usage = "indir outdir [type]" },
        [ "mergeCelToDir"] = { cmd = "mergeCelToDir", nargs = 3, usage = "indir imgdir outdir [mergeAll]" },

        [ "updateRawText"] = { cmd = "updateRawText", nargs = 2, usage = "indir txtdir" }
}

local function run(cmd, ...)
        if cmd and cmdlist[cmd] then
                local com = cmdlist[cmd]
                if #arg - 1 >= com.nargs then
                        local ttype = type(com.cmd)
                        if ttype == "string" then
                                return a17[com.cmd](...)
                        elseif ttype == "function" then
                                return com.cmd(...)
                        else
                                return nil, "ERROR: not supported type: " .. ttype
                        end
                else
                        io.stderr:write(string.format("Usage: lua a17rt.lua %s %s\n", cmd, com.usage))
                        return nil, string.format("ERROR no enough args: %d < %d", #arg - 1, com.nargs)
                end
        else
                return nil, "ERROR no such command: " .. tostring(cmd)
        end
end

assert(run(...))
