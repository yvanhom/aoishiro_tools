{
  {
    {
      "SPEAK FNQQAAABLAA=",
      "L1",
      "　ナミルートの封印が解除されました。",
      "Seal on Nami's Route has been unlocked. "
    }
  },
  {
    {
      "SPEAK FNQQAAABMAA=",
      "L1",
      "　カヤルートの封印１が解除されました。",
      "Kaya Route - Seal One has been unlocked. "
    }
  },
  {
    {
      "SPEAK FNQQAAABMAA=",
      "L1",
      "　カヤルートの封印２が解除されました。",
      "Kaya Route - Seal Two has been unlocked. "
    }
  },
  {
    {
      "SPEAK FNQQAAABMAA=",
      "L1",
      "　コハクルートの封印が解除されました。",
      "Seal on Kohaku's Route has been unlocked. "
    }
  },
  {
    {
      "SPEAK FOwRAAABNAA=",
      "L1",
      "　グランドルートの封印１が解除されました。",
      "Grand Route - Seal One has been unlocked. "
    }
  },
  {
    {
      "SPEAK FNQQAAABNAA=",
      "L1",
      "　グランドルートの封印２が解除されました。",
      "Grand Route - Seal Two has been unlocked. "
    }
  },
  {
    {
      "SCENE EQAAAACcAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇青い城へ―\―\乙姫を残して　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ To the Blue Castle - Leaving Behind a Young     Princess - Recall                      #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇鬼来たる　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ An Oni Comes  �想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACIAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇眠り姫　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Sleeping Beauty - Recall                                             #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇深い処へ沈み逝く　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Sinking Down to One's Death - Recall                                           #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇青と白の空の下へ　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Beneath the Blue and White Sky - Recall                                        #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACQAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇既視と重なる　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Additional Deja Vu - Recall  ―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇白む彼方の日が射す前に　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Before the Sun Shines Over There - Recall                                            #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇前門の虎・後門の艮　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Tiger at the Front Gate, Ox at the Rear Gate    - Recall  \\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇彼女の内に在る《力》　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ ◇ The 《Power》 Within Her - Recall  \\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACQAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇牛飲する摩多牟　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Matamu Gorges - Recall                                                       #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇歪む慟哭　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Distorted Lament - Recall  ―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇あなたの形の心の隙間　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Gap in the Shape of You in My Heart -       Recall                             #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACcAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇わだつみにそのみをささぐ　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ An Offering to the Sea God - Recall                                        ―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACcAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇期待のホープがもうひとり　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Another Star of Hope - Recall  ―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#c―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇鬼の群れ　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ A Throng of Oni - Recall                                               #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇常咲きの花の落ちるとき　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ When the Tokosaki Flowers Fall - Recall                                              #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇別れの季節　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ A Season for Farewells - Recall  ―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACIAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇禁門　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Forbidden Gate - Recall                                            #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇海淵に住まう夷の王　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Barbarian King that Lives in the Deep Sea - Recall  \\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇一切の希望を捨てよ　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Abandon All Hope - Recall                                                        #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACQAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇剣鬼と既視感　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Kenki and Deja Vu - Recall  0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇根方の祭儀　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Nekata Ritual - Recall  \\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇流れに流れ　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Carried Away By the Current - Recall  \\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇はじめての冬休み　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ First Winter Vacation - Recall                                                 #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇流れ行く時流れる景色　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ When Leaving, the Scenery Passes By - Recall                                       #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇海の藻屑　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ A Watery Grave - Recall                                                #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇花の赤く散るらむ　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Flowers Fall in a Flurry - Recall                                          #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇満ち行く潮　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Rising Tide - Recall                                                 #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACQAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇嵐に散り落つ　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Flung Around and Dropped by the Storm -         Recall  \\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇海神の犠牲　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Sacrifice to the Sea God - Recall  ―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇隻眼白髪の鬼使い　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The One-Eyed White-Haired Wielder of Oni -      Recall  \\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACQAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇閉ざされる未来　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Closed Off Future - Recall  \\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACIAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇好敵手　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Worthy Opponent - Recall  \\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACcAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇夢の縁（よすが）を洗う海風　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Sea Breeze that Cleanses Remnants of the    Dream - Recall                           #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇双鬼相打つ　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Duel Between Two Oni - Recall                                            #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇丑寅の路　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Road in the Ox-Tiger Direction - Recall                            #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇椿の如く石榴の如く　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Just like a Tsubaki, Just like a Pomegranate                                     #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACQAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇《剣》は沈まず　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The《Sword》Won't Sink                                                       #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇踏み石渡るは誰そ彼　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Crossing over the Stepping Stones at Twilight                                    #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇海の石榴となりて　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Turning Into an Ocean Pomegranate                                              #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇力及ばず　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Cannot Summon Up the Power  \\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇《剣》憑き　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇  Possessed by the《Sword》                                               #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ふたりを繋ぐ一振りの刃　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ One Swing of a Blade to Connect the Two of Us                                        #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACQAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇その瞳に映る星　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Stars Reflected in Her Eyes  ―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇不器用ものが針を持ち　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ An Awkward Person Holds a Needle  cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇散り逝く宿命に抗えど　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Defy Fate to Fade Away to Oblivion                                                 #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACgAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇鉄（くろがね）の式神（しき）　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Shikigami of Steel                                                                         #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇その面影は遙か遠くに　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Traces of Her Are Receding Into the         Distance                           #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇摩多牟とオニワカ　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Matamu and Oniwaka                                                             #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACQAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇九郎、狂う。　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Kurou, Kuruu (Kurou Goes Insane)  \\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇未だ眠らず　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Not Yet Slumbering                                                       #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACMAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇群がる小鬼　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ A Crowd of Small Oni  \\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACYAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇《剣》は鬼王の黒き手に　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Oni King Holds the《Sword》in His Black  Hand     \\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACUAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇嵐の中を越えていく　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Crossing Over in the Middle of a Storm  \\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACQAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇蜘蛛討ち切断　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ Kumouchi is Severed                                                        #cr0　"
    }
  },
  {
    {
      "SCENE EQAAAACcAA==",
      "L1",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇うなさかをこえるきざはし　―\―\回想―\―\#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0　",
      "　#cr0―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\―\#cr0◇ The Steps Leading Past Unasaka                                                         #cr0　"
    }
  }
}